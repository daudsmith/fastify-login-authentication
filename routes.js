let users = require('./controller/users');
let auth = require('./controller/auth');

async function routes (fastify, options) {

    //Route Ujicoba
    fastify.get('/', function (request, reply) {
        reply.send({message: 'Hello World', code: 200});
    });

    fastify.post('/api/users/register', users.register);

    fastify.post('/api/users/login', users.login);

    fastify.post('/api/token', auth.createToken);

}

module.exports = routes;